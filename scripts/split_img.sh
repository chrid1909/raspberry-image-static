#!/usr/bin/env bash


if [ "x" == "x$1" ]; then
	echo "usage: $0 <imagefile>"
	exit 1
fi

IMAGE=$1
OFFSET=`echo $(/sbin/fdisk -l $IMAGE | tail -n 1 | cut -c40-49)`

echo "Splitting $image at offset $OFFSET"

dd if=$IMAGE of=part1.img bs=512 count=$OFFSET
dd if=$IMAGE of=part2.img skip=$OFFSET bs=512

ls -alh part?.img $IMAGE
